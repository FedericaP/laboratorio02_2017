package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	
	private static final int MAX_ROLLS = 20;
	private int score = 0;
	private int[] rolls = new int[20];
	private int currentRoll = 0;
	private int conta= 1;

	@Override
	public void roll(int pins) {
		rolls[currentRoll++] = pins;

	}

	@Override
	public int score() {
		int score = 0;
		for(int currentRoll=0; currentRoll<MAX_ROLLS; currentRoll++){
			if(isStrike(currentRoll) && (conta%2 !=0))
				score += rolls[currentRoll+2]+rolls[currentRoll+1]+rolls[currentRoll];
			else if(isSpare(currentRoll) && (conta%2 !=0))
				score += rolls[currentRoll+2]+rolls[currentRoll];
			else
				score += rolls[currentRoll];
			conta++;
		}
		return score;
	}

	private boolean isSpare(int currentRoll) {
		return currentRoll<MAX_ROLLS-1 && rolls[currentRoll] + rolls[currentRoll+1] == 10;
	}
	
	private boolean isStrike(int currentRoll) {
		return rolls[currentRoll] == 10;
	}

}
